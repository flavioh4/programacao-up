// Flavio RL Jr.
//
// Biblioteca:
// http://www.cplusplus.com/reference/cstdio/
//
// Git: 
// https://bitbucket.org/flavioh4/programacao-up/
#include <stdio.h>


int main(){


	float nota;

	scanf("%f",&nota);


	if(nota >= 7){
		
		if(nota > 9){
			printf("aprovado com louvor");
		}else{
			printf("aprovado");
		}

	}else{

		printf("reprovado");
	}
	
	

	return 0;
}
#include <stdio.h>

//Bibliotecas
//http://www.cplusplus.com/reference/cstdio/

int main(){


	int n1,n5,n10,n50,n100,v;

	printf("Qual valor:");
	scanf("%d",&v);

	n100 = v/100;
	n50 = (v%100)/50;
	n10 = ((v%100)%50)/10;
	n5 = (((v%100)%50)%10)/5;
	n1 = ((((v%100)%50)%10)%5);
	
	printf("\n%d notas de 1, \n%d notas de 5, \n%d notas de 10, \n%d notas de 50, \n%d notas de 100\n",n1,n5,n10,n50,n100);

	return 0;
}
#include <stdio.h>

//Bibliotecas
//http://www.cplusplus.com/reference/cstdio/

int main(){


	int h,m,s,t;

	printf("Tempo do evento em segundos:");
	scanf("%d",&t);


	h = t/3600;
	m = (t%3600)/60;
	s = (t%3600)%60;
	
	printf("Tempo de %dh:%dm:%ds\n",h,m,s);


	return 0;
}
#include <stdio.h>  /* funções de entrada e saída */
#include <math.h>  /* funções matemáticas */
#define  PI  3.14159 /* constante */


int main(void) {

int raio;
float perimetro, area;

printf("Entre com o valor do raio:" );

scanf("%d", &raio);

perimetro = 2 * PI * raio;

area = PI * pow(raio, 2);

printf("O perimetro da circunferencia eh %.2f\n", perimetro);
printf("A area eh %.2f\n", area);

return 0;

}